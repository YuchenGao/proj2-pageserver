from flask import Flask,render_template

app = Flask(__name__)



@app.route('/')
def index():
    return forbidden(403)


@app.route('/<path:paths>')
def Find_Paths(paths):

    if "//" in paths or "~" in paths or ".." in paths:
        return forbidden(403)
    else:
        try:
            return render_template(paths), 200
        except:
            return not_found(404)
    


@app.errorhandler(403)
def forbidden(error):
    return render_template("403.html",title ="Forbidden"), 403


@app.errorhandler(404)
def not_found(error):
    return render_template("404.html",title = "Not Found"), 404


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
